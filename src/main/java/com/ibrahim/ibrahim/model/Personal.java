package com.ibrahim.ibrahim.model;


import java.util.Date;
import javax.annotation.processing.Generated;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "personals")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Personal {
    @Id
    private String id;
    @Field(name = "name",type = FieldType.Text)
    private String name;
    @Field(name = "last_name",type = FieldType.Text)
    private String lastName;
    @Field(name = "address",type = FieldType.Text)
    private String address;
//    @Field(name = "birthdate",type = FieldType.Date)
//    private Date date;
}

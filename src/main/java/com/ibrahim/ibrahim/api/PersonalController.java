package com.ibrahim.ibrahim.api;


import com.ibrahim.ibrahim.model.Personal;
import com.ibrahim.ibrahim.repository.PersonalRepository;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor//or constructor or autowire
@RestController
@RequestMapping
public class PersonalController {
    private final PersonalRepository personalRepository;

//    public PersonalController(PersonalRepository personalRepository) {
//        this.personalRepository = personalRepository;
//    }
@PostConstruct
public void initss(){
    Personal personal= new Personal();
    personal.setId("123ibrahim");
    personal.setName("Ibrahim");
    personal.setLastName("Ibrahim");
    personal.setAddress("Ibrahim");
//    personal.setDate(Calendar.getInstance().getTime());
    personalRepository.save(personal);
}
    @GetMapping("/{search}")
    public ResponseEntity<List<Personal>> getPersonal(@PathVariable String search){
        List<Personal> list=personalRepository.getByCustomQuery(search);
        return ResponseEntity.ok(list);
    }

    @GetMapping("/per/{search}")
    public List<Personal> getPersonal2(@PathVariable String search){
        List<Personal> list=personalRepository.findByNameLike(search);
        return list;
    }



}

package com.ibrahim.ibrahim.repository;

import com.ibrahim.ibrahim.model.Personal;
import java.util.List;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonalRepository extends ElasticsearchRepository<Personal,String> {

    @Query("{\"bool\": {\"must\":[{\"match\":{\"name\": \"?0\"}}]}}")
    List<Personal> getByCustomQuery(String search);

    List<Personal> findByNameLike(String name);



}
